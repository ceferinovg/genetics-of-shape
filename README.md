##########################################

SCRIPTS ON THE GENETICS OF SHAPE VARIATION

##########################################

This is an informal repository where I'll gather up some of the scripts I've used for my work on the genetics of shape.

Disclaimer: These scripts are by no means the final scripts used for my articles.

Because I'm uploading these scripts long time after the publication of the articles, I can't upload the final versions,
where my collaborators have given some input, without their permission. Because they had an important input in these
scripts, important changes might have been done from the analyses shown here and the ones performed for the articles.

So yes, these are dirty and messy scripts I've been developing myself until a certain moment. I upload them here with
the hope of improving them in the future in case I want to reuse them and because, why not, some lines may help some
desesperate researcher (as myself).

This previous warning doesn't mean that you can't comment, send pull-requests or interact with me if you're interested in 
the content of these scripts. 

I'll be more than happy to discuss the analyses or, more generally, the genetics of shape :)